﻿#ifndef IDENTIFICATION_H
#define IDENTIFICATION_H
#include <string>
using namespace std;

class Person
{
public:

    Person(string,string);
    Person();
    void setPerson(string,string);
    void printId();
    void run();
//****************************************************

private:

    string name_;
    string lastName_;
};

#endif // IDENTIFICATION_H
