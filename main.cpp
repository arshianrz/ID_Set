#include "identification.h"
#include <string>
using namespace std;

int main()
{
    Person user1("arshia","nozary");
    user1.printId();

    Person user2("ali","mohamadi");
    user2.printId();

    Person user3;
    user3.run();

    return 0;
}
